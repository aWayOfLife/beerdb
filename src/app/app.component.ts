import { Component } from '@angular/core';
import { IBeer, IBeerDetailed } from './interfaces/beer';
import { IImage } from './interfaces/image';
import { BeerService } from './services/beer.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  beerList:IBeer[];
  beerListDetailed:IBeerDetailed[]=[];
  images: IImage[];
  p: number = 1;
  search:string = '';
  filter:IBeerDetailed = { beer:{name:'', abv:'', ibu:'', style:'', id:null, ounces:null} , image:''};

  constructor(private beerService:BeerService){}

  ngOnInit(){
    this.getAllImages();
    this.getAllBeer();
  }

  getAllImages(){
    this.beerService.getAllImages().subscribe(data =>{
      if(data){
        this.images = data;
      }
    },error =>{console.log(error)})
    
  }

  getAllBeer(){
    this.beerService.getAllBeer().subscribe(
      result =>{
        if(result){
          this.beerList = result.slice(0,100);
          for(let i=0;i<this.beerList.length;i++){
            this.beerListDetailed[i]={
              beer:this.beerList[i],
              image: this.images[i%4+1].image
            }
          }
         
        }
        else{
          console.log("No data found");
        }
      },
      error =>{
        console.log(error);
      }
    )
  }

  onKeyUpEvent(event: any){

    if(event.target.value!=''){
      this.p=1;
    }
    

  }
}
