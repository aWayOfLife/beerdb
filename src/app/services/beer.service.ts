import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IBeer } from '../interfaces/beer';
import { IImage } from '../interfaces/image';

@Injectable({
  providedIn: 'root'
})
export class BeerService {
  apiUrl = environment.apiUrl;
  imageUrl = environment.imageUrl;

  constructor(private http:HttpClient) { }

  getAllBeer():Observable<IBeer[]>{
    return this.http.get<IBeer[]>(this.apiUrl);
  }

  getAllImages():Observable<IImage[]>{
    return this.http.get<IImage[]>(this.imageUrl);
  }

}
