import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { IBeer, IBeerDetailed } from '../interfaces/beer';


@Component({
  selector: 'app-beer-card',
  templateUrl: './beer-card.component.html',
  styleUrls: ['./beer-card.component.css']
})
export class BeerCardComponent implements OnInit {
  @HostBinding('style.--target-height') private targetHeight: string;
  @Input() beer:IBeerDetailed;
  url:string;

  constructor() { }

  ngOnInit(): void {
    
    console.log()
    this.targetHeight = (+this.beer.beer.abv)*25+"rem";
  }

}
