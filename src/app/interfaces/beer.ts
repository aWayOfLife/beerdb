export interface IBeer {
    abv: string;
    ibu: string;
    id: number;
    name: string;
    style: string;
    ounces: number;
}
export interface IBeerDetailed {
    beer:IBeer;
    image: string;
}