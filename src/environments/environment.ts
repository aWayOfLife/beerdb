// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCz3tssuqFRJSmCsnue5kmVjEfp0towNm0",
    authDomain: "beerdb-41e20.firebaseapp.com",
    databaseURL: "https://beerdb-41e20.firebaseio.com",
    projectId: "beerdb-41e20",
    storageBucket: "beerdb-41e20.appspot.com",
    messagingSenderId: "747987037252",
    appId: "1:747987037252:web:995a46b9efd8a7a9bf2011"
  },
  apiUrl:"https://s3-ap-southeast-1.amazonaws.com/he-public-data/beercraft5bac38c.json",
  imageUrl:"https://s3-ap-southeast-1.amazonaws.com/he-public-data/beerimages7e0480d.json"

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
