export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCz3tssuqFRJSmCsnue5kmVjEfp0towNm0",
    authDomain: "beerdb-41e20.firebaseapp.com",
    databaseURL: "https://beerdb-41e20.firebaseio.com",
    projectId: "beerdb-41e20",
    storageBucket: "beerdb-41e20.appspot.com",
    messagingSenderId: "747987037252",
    appId: "1:747987037252:web:995a46b9efd8a7a9bf2011"
  },
  apiUrl:"https://s3-ap-southeast-1.amazonaws.com/he-public-data/beercraft5bac38c.json",
  imageUrl:"https://s3-ap-southeast-1.amazonaws.com/he-public-data/beerimages7e0480d.json"
};
